<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class itemlist extends Model
{
    protected $fillable = [
        'type' , 'title' , 'discription' , 'price' , 'image' 
    ];

public function lists(){
   return $this->belongsTo(lists::class);
}
    

}
