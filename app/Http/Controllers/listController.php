<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class listController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $forminput=request()->validate([
            'type' => 'required',
        ]);
        $forminput=$request->all();

        \App\lists::create($forminput);

        return redirect()->back();
    }


    public function delet(Request $request)
    {
       //echo($id);
        $forminput=request()->validate([
        'type' => 'required'
        ]);
        $forminput=$request->all();
        $abc= $forminput['type'];
        $list=\App\lists::where('type' , '=' , $abc)->get();
       // $ida=0;
        $lis=null;
        foreach($list as $l)
        {
           //$ida= $l->id;
           $lis=$l;
        }
       // echo($lis);
        $lis->delete();
        $itmlist=\App\itemlist::where('type' , '=' , $abc)->get();
        foreach($itmlist as $i)
        {
            $image=$i->image;
            $image_path = "images/".$image;  
            if (file_exists($image_path)) {

                unlink($image_path);
        
                
               }
            $i->delete();   
        }

        //$itmlist->delete();

       //$data=\App\lists::find($id);
      // $data->delete();
         return redirect('/');
    }

}
