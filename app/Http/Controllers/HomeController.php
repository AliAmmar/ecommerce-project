<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        /*$users=\App\lists::all();
        $use =array();

        foreach ($users as $u) {
        $use = array_add($use, $u->type,$u->type);

        }
        $user=array_values($use);*/
        $user=\App\lists::pluck('type','type');

        //dd($user);
        return view('home',compact('user') );




        //return view('home');
    }

    
}
