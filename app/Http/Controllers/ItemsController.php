<?php

namespace App\Http\Controllers;
use App\itemlist;
use Illuminate\Http\Request;
use App;

class ItemsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {

        $user=itemlist::all();
       // $use=\App\lists::all();

        //dd($user);
        //dd($use);
        return view('view',compact('user') );
    }
    public function detail($id)
    {
        $use=\App\itemlist::find($id);
        return view('viewDetail',compact('use') );
    }
    public function store(Request $request)
    {
        $forminput=request()->validate([
            'type' => 'required',
            'title' =>' required',
            'discription' => 'required',
            'price' => 'required',
            'image' => 'required | image',
        ]);


        $forminput=$request->all();

        $image=$request->image;

        if($image){
            $imageName=$image->getClientOriginalName();
            $image->move('images',$imageName);
            $forminput['image']=$imageName;

        }

       // \App\itemlist::create($forminput);
        $abc=$request->type;
        $list=\App\lists::where('type' , '=' , $abc)->get();
        $ida=0;
        $lis;
        foreach($list as $l)
        {
           $ida= $l->id;
           $lis=$l;
        }
        //echo $ida;
        //echo $lis;
        //$list=\App\lists::with('itemlists')->where('type','=', $abc)->get();

        $lis->itemlists()->create($forminput);
       // $id=\App\lists::create($forminput);
       // dd( $id);
       // echo ($id['id']);

           // $forminput['list_id']=$id['id'];
           // echo($forminput['list_id']);
            //\App\itemlist::create($forminput);
        //dd($list);
        //dd(auth()->user());
        //echo(gettype(auth()->user()));
        //echo (gettype($list));

        //auth()->user()->itemlists()->create($forminput);
       return redirect()->back();//->with('succes', 'Data has been successfully sent!');
    }
    public function delete()
    {
        $user=\App\itemlist::all();
        return view('viewDel',compact('user') );
    }

    public function delet($id)
    {
       //echo($id);
       $data=\App\itemlist::find($id);
       //echo($data);
       $image=$data->image;
       $image_path = "images/".$image;  // Value is not URL but directory file path
       echo $image_path;

       if (file_exists($image_path)) {

        unlink($image_path);


       }
       $data->delete();
       return redirect('/');
    }

    public function UpdateView()
    {
        $user=\App\itemlist::all();
        return view('UpView',compact('user') );

    }

    public function update($id)
    {

        $use=\App\itemlist::find($id);
        $list=\App\lists::pluck('type','type');
        return view('viewUpdate',compact('use','list') );

    }
    public function updat($id)
    {
    }

}
