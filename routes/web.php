<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'ItemsController@show');
route::post('/add','ItemsController@store');
route::post('/addtype','listController@store');
route::get('/delete','itemsController@delete');
route::get('/delet/{id}','itemsController@delet');
route::post('/deletetype','listController@delet');
route::get('/update','itemsController@UpdateView');
route::get('/update/{id}','itemsController@update');
route::get('/updat/{id}','itemsController@updat');
route::get('/detail/{id}','itemsController@detail');
//route::view('aa', 'detail');