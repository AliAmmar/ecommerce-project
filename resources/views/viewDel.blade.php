@extends('layouts.app')

@section('content')
    
<div class="d-flex flex-wrap bg-light">
    @forelse ($user as $use)
        
        <div class="p-2 border" >
        <img src="{{ asset('images/' . $use->image) }}" height="200" width="200" />
        

        <h4 style="color: cornflowerblue">{{ $use->title }}</h4>
        <button class="btn btn-warning"><a href="delet/{{ $use->id}}">delete</a></button>
        </div>    
    @empty
        <li>No record found!</li>
    
    @endforelse
</div>   
@endsection
