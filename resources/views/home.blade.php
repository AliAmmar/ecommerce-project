@extends('layouts.app')

@section('content')

<div class="d-flex flex-wrap bg-light">


    <!--         Add Item Type         -->

    <div class="p-2 border" >
<div class="align" style="padding-right: 55%">
    {!! Form::open(['url' => 'addtype' , 'method' =>'post' ]) !!}
    @csrf
            <h2>Add a new Item  Type !</h2>
            <div class="form-group">
                {{Form::label('type','type')}}
                {{Form::text('type')}}
            </div>
            <p style="color: red"> @error('type') {{$message}} @enderror</p>
            <div >
                <button type="submit" class="btn btn-success">ADD</button>
            </div>
        {!! Form::close() !!}
</div>   

    </div>

    <div class="p-2 border" style="background-color: rgb(167, 161, 161)" >

    
        <!--          Add Item          -->


    <div class="col-sm-4" >
        {!! Form::open(['url' => 'add' , 'method' =>'post' , 'files' => true ]) !!}

        
            @csrf
            <h2>Add a new Item !</h2>
            <div class="form-group">
                {{Form::label('type','type')}}
                {{Form::select('type', $user)}}
            </div>
            <p style="color: red"> @error('type') {{$message}} @enderror</p>
            <div class="form-group">
                {{Form::label('title','Title')}}
                {{Form::text('title')}}
            </div>
            <p style="color: red">@error('title') {{$message}} @enderror</p>
            

            <div class="form-group">
                {{Form::label('discription','Discription')}}
                {{Form::text('discription')}}
            </div>
            <p style="color: red">@error('discription') {{$message}} @enderror</p>
            <div class="form-group">
                {{Form::label('price','Price')}}
                {{Form::number('price')}}
            </div>
            <p style="color: red">@error('price') {{$message}} @enderror</p>
            <div class="form-group">
                {{Form::label('image','Image')}}
                {{Form::file('image')}}
            </div>
            <p style="color: red">@error('image') {{$message}} @enderror</p>
            <div >
                <button type="submit" class="btn btn-success">ADD</button>
            </div>
        {!! Form::close() !!}
    </div>   
    


    </div>

    <!--          Delete item                -->
    <div class="p-2 border" >
        <div class="align" style="padding-right: 55%">
            {!! Form::open(['url' => 'deletetype' , 'method' =>'post' ]) !!}
            @csrf
                    <h2>Delete an Item Type !</h2>
                    <div class="form-group">
                        {{Form::label('type','type')}}
                        {{Form::select('type', $user)}}
                    </div>
                    <p style="color: red"> @error('type') {{$message}} @enderror</p>
                    <div >
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                {!! Form::close() !!}
        </div>   
        
            </div>




</div>

@endsection
