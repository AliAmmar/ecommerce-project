@extends('layouts.app')

@section('content')
    
<div class="d-flex flex-wrap bg-light">
        
    <div class="col-sm-4" >
        {!! Form::open(['url' => 'updat' , 'method' =>'post' , 'files' => true ]) !!}

           @csrf
            <h2>Add a new Item !</h2>
            <div class="form-group">
                {{Form::label('type','Type')}}
                {{Form::select('type', $list)}}
            </div>
            <p style="color: red"> @error('type') {{$message}} @enderror</p>
            <div class="form-group">
                {{Form::label('title','Title')}}
                {{Form::text('title', $use['title'])}}
            </div>
            <p style="color: red">@error('title') {{$message}} @enderror</p>
            

            <div class="form-group">
                {{Form::label('discription','Discription')}}
                {{Form::text('discription',$use['discription'])}}
            </div>
            <p style="color: red">@error('discription') {{$message}} @enderror</p>
            <div class="form-group">
                {{Form::label('price','Price')}}
                {{Form::number('price',$use['price'])}}
            </div>
            <p style="color: red">@error('price') {{$message}} @enderror</p>
            <div class="form-group">
                {{Form::label('image','image')}}
                {{Form::file('image'  )}}
            </div>
            <p style="color: red">@error('image') {{$message}} @enderror</p>
            <div >
                <button type="submit" class="btn btn-success">Update</button>
            </div>
        {!! Form::close() !!} 
    </div>     
    
</div>   
@endsection
