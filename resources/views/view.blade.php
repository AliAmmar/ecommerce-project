@extends('layouts.app')

@section('content')
    
<div class="d-flex flex-wrap bg-light" >
    @forelse ($user as $use)
        
        <div class="b-3 ">
            <a href="{{ URL('detail/'.$use->id )}}" style="margin-left: 10px"><img src=
                "{{ asset('images/' . $use->image) }}" height="200" width="200" /></a>
        <a href="{{ URL('detail/'.$use->id )}}"><h4>{{ $use->title }}</h4></a>
        
        </div>
        <br>    
    @empty
        <li>No record found!</li>
    
    @endforelse
</div>   
@endsection
